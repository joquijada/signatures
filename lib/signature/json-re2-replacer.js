const debug = require('debug')('dls:Signature#toJson~jsonRe2Replacer')
const RE2 = require('re2')

const isRe2 = (value) => value instanceof RE2

const isRegExp = (value) => value instanceof RegExp

const isExpression = (value) => isRe2(value) || isRegExp(value)

const jsonRe2Replacer = (key, value) => {
  debug('Convert %s to string: %s', key, isExpression(value))
  if (isExpression(value)) {
    return value.toString()
  }
  return value
}

module.exports = jsonRe2Replacer
