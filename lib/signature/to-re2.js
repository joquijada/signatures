/* eslint-disable max-statements,require-unicode-regexp */

const debug = require('debug')('dls')
const RE2 = require('re2')
const { REGEX } = require('./valid-signature-types')

/**
 * Create a valid re2 regular expression.
 *
 * @function toRe2
 *
 * @param {{pattern, type}} {*} - A Signature or object literal.
 * @param {string} {pattern} - A string or regular expression.
 * @param {string} {type} - Object data-type identifier.
 * @returns {string|RegExp|RE2} - The original pattern value or
 *
 * - An RE2 instance or
 * - A regular expression when ever RE2.constructor throws an error.
 *
 * @see {@link https://github.com/uhop/node-re2#readme|uhop/node-re2 README}
 * @see {@link https://github.com/google/re2#readme|google/re2 README}
 * @see {@link https://github.com/google/re2/wiki|google/re2 Wiki (documentation)}
 */

const toRe2 = ({ pattern, type }) => {
  let ptn = pattern
  const flags = 'gimu'
  debug(`Evaluating ${type} "${pattern}"`)
  if (type === REGEX) {
    if (typeof pattern === 'string') {
      [ ptn ] = pattern.split(`/${flags}`)
      ptn = ptn.replace(/^\//, '')
    }
    try {
      ptn = new RE2(ptn, flags)
    } catch (err) {
      /* istanbul ignore next */
      debug(err.message)
    }
  }
  return ptn
}

/* eslint-enable max-statements,require-unicode-regexp */

module.exports = toRe2
