const signature = require('..')
const signatures = require('../../../signatures.json')

const { Signature } = signature

describe('module', () => {
  describe('signature', () => {
    describe('.factory', () => {
      it('generates a Signature instance', () => {
        const defn = signature.factory()
        expect(defn).toBeInstanceOf(Signature)
      })
    })
    describe('.toArray', () => {
      describe('is a static method that', () => {
        it('converts a JSON string to an array of Signatures', () => {
          const json = JSON.stringify(signatures)
          const signatureList = signature.toArray(json)
          expect(signatureList).toBeInstanceOf(Array)
          expect(signatureList.every((item) => item instanceof Signature))
        })
        it('converts an object literal into an array of Signatures', () => {
          const signatureList = signature.toArray(signatures)
          expect(signatureList).toBeInstanceOf(Array)
          expect(signatureList.every((item) => item instanceof Signature))
        })
      })
    })
    describe('.nullSignature', () => {
      it('is a NullObject implementation for Signatures', () => {
        expect(signature.nullSignature).toBeDefined()
      })
    })
    describe('.Signature', () => {
      it('constructs Signature intances', () => {
        const { Signature } = signature
        const defn = new Signature()
        expect(defn).toBeInstanceOf(Signature)
      })
    })
    describe('.validParts', () => {
      it('is an enum with Signature@part constants', () => {
        expect(signature.validParts.CONTENTS).toBe('contents')
        expect(signature.validParts.EXTENSION).toBe('extension')
        expect(signature.validParts.FILENAME).toBe('filename')
        expect(signature.validParts.PATH).toBe('path')
        expect(signature.validParts.UNKNOWN).toBeNull()
      })
    })
    describe('.validTypes', () => {
      it('is an enum with Signature@type constants', () => {
        expect(signature.validTypes.MATCH).toBe('match')
        expect(signature.validTypes.REGEX).toBe('regex')
        expect(signature.validTypes.UNKNOWN).toBeNull()
      })
    })
  })
})
