/**
 * Enum of valid Signature parts.
 *
 * @memberof dataLossSignatures
 */

/**
 * Enum of valid Signature parts.
 *
 * @typedef {enum} validSignatureParts
 * @readonly
 * @enum {string}
 */

const validSignatureParts = {
  'CONTENTS': 'contents',
  'EXTENSION': 'extension',
  'FILENAME': 'filename',
  'PATH': 'path',

  /** @type {null} */

  'UNKNOWN': null
}

module.exports = validSignatureParts
