/**
 * Enum of valid Signature types.
 *
 * @memberof dataLossSignatures.signature
 */

/**
 * Enum of valid Signature types.
 *
 * @typedef {enum} validSignatureTypes
 * @readonly
 * @enum {string}
 */

const validSignatureTypes = {
  'MATCH': 'match',
  'REGEX': 'regex',

  /** @type {null} */

  'UNKNOWN': null
}

module.exports = validSignatureTypes
