/* eslint-disable node/no-unpublished-require */
/**
 * A public API for accessing signatures.json, the single data source
 * for types of data-leakage, as well as how to identify them
 * programmatically.
 *
 * @module @data-leakage-protection/signatures
 * @alias dataLossSignatures
 */

/**
 * Defines the prototype for IT secret detection.
 *
 * @member {class} Signature
 *
 * @see module:dataLossSignatures
 *
 * @property {object} [params=nullSignature] - Property values for a new Signature.
 * @property {string|null} params.caption -Summarizes the data-leakage detection defintion.
 * @property {string|null} params.description -Optionally distinguishes signatures
 * with more details.
 */

/*
 *
 *
 * @property {Array.<Signature>} signatures - All valid Signature instances.
 * @property {object} nullSignature - A NullObject implementation of Signature.
 * @property
 * @property {validParts} validParts - An enum of acceptable Signature@part values.
 * @property {enum} validTypes - An enum of acceptable Signature@type values.
 */

const debug = require('debug')('dls')

require('./env-config')

const signature = require('./signature')
const signatures = require('../signatures.json')

/**
 * Enum of valid Signature parts.
 *
 * @enum {object} validParts
 * @var
 * @static
 * @alias dataLossSignatures.validParts
 *
 * @property {string} CONTENTS 'contents'
 * @property {string} EXTENSION 'extension'
 * @property {string} FILENAME 'filename'
 * @property {string} PATH 'path'
 * @property {null} UNKNOWN null
 */

/**
 * @method factory - Create a new Signature instance.
 * @memberof dataLossSignatures
 *
 * @param {object} [params=nullSignature] - The Signature instance property values to set.
 * @memberof dataLossSignatures.signature
 * @returns {Signature}
 */

/**
 * @description
 * Invoke HTTP GET request to retrieve signatures.json.
 *
 * @var get
 * @function
 * @static
 *
 * @param {object} options
 * @param {string}[options.baseUrl=https://gitlab.com/api/v4] - The Gitlab API endppoint.
 * @param {string} [options.filePath=signatures.json] - The Gitlab API endppoint.
 * @param {Object<string, Function[]>} [options.hooks] - Hooks allow modifications during the request lifecycle. Hook functions may be async and are run serially.
 * @param {Array.<Function>} [options.hooks.afterResponse] - Methods that transform the response.
 * @param {Array.<Function>} [options.hooks.beforeError] - Methods that transform the HTTP Error.
 * @param {string} [options.projectId=10416318] - The Gitlab Project identifier.
 * @param {string} [options.ref=master] - The branch, tag, or commit-ish.
 * @param {string|undefined} [options.token] - A Gitlab personal access token with "api" or "read_repository" scope.
 * @returns {Promise.<string|GotError>} - A JSON-encoded array of signatures, or a GotError.
 */

const dataLossSignatures = {

  ...signature
}

debug('Generate an array of Signature objects from JSON')
dataLossSignatures.signatures = signature.toArray(signatures)

debug('Export module dataLossSignatures.')
module.exports = dataLossSignatures
