const { EXTENSION, FILENAME, PATH } =
  require('../../signature/valid-signature-parts')
const { MATCH } = require('../../signature/valid-signature-types')
const RE2 = require('re2')

const matchTypeExpressionMap = new Map([
  [
    EXTENSION,
    (signature) => `${signature.pattern}$`
  ],
  [
    FILENAME,
    (signature) => signature.pattern
  ],
  [
    PATH,
    (signature) => `${signature.pattern.replace(new RE2('/', 'gimu'), '/')}`
  ]
])

const toExpression = (signature) => {
  if (signature.type === MATCH) {
    return matchTypeExpressionMap.get(signature.part)(signature)
  }
  return signature.pattern.toString()
}

module.exports = toExpression
