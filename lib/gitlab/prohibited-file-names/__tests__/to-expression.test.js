const toExpression = require('../to-expression.js');

describe('toExpression', () => {
  describe('converts Signature.prototype.type', () => {
    describe('MATCH', () => {
      test('to an RE2 expression {string}', () => {
        const signature = {
          part: 'path',
          pattern: '/User/account/go/lib',
          type: 'match'
        }
        const expression = toExpression(signature)
        expect(expression).toBe(signature.pattern)
      })
    })
    describe('REGEX', () => {
      test('to {string}', () => {
        const signature = {
          part: 'path',
          pattern: '/User/account/go/lib',
          type: 'regex'
        }
        const expression = toExpression(signature)
        expect(expression).toBe(signature.pattern)
      })
    })
  })
})
