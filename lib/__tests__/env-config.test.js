/* eslint-disable max-lines-per-function */
/* eslint-disable no-console */
/* eslint-disable no-empty-function */
const dotenvExtended = require('dotenv-extended')
const envConfig = require('../env-config')

describe('envConfig', () => {
  it('loads .env files', () => {})
  describe('and optionally loads defaults and a schema', () => {
    describe('.env.defaults', () => {
      it('assigns default values to ENV variables', () => {
        /*
         * GITLAB_ENDPOINT=https://gitlab.com/api/v4
         * GITLAB_PROJECT_ID=11264727
         * GITLAB_REF=master
         * GITLAB_RESOURCE=signatures.json
         */

        expect(envConfig).toBeDefined()
        expect(envConfig.GITLAB_ENDPOINT).toBe('https://gitlab.com/api/v4')
        expect(envConfig.GITLAB_PROJECT_ID).toBe('12108526')
        expect(envConfig.GITLAB_REF).toBe('master')
        expect(envConfig.GITLAB_RESOURCE).toBe('signatures.json')
      })
    })
    describe('.env.schema', () => {
      let defaultOptions = null
      let envConf = null

      beforeEach(() => {
        defaultOptions = {
          'assignToProcessEnv': true,
          'defaults': '.env.defaults',
          'encoding': 'utf8',
          'errorOnExtra': false,
          'errorOnMissing': false,
          'includeProcessEnv': false,
          'overrideProcessEnv': false,
          'path': '.env',
          'schema': '.env.schema',
          'silent': true
        }
      })
      afterEach(() => {
        defaultOptions = null
        envConf = null
      })
      describe('options', () => {
        it('declares where ENVVARs are defined, and how to use them', () => {
          envConf = dotenvExtended.load(defaultOptions)
          expect(envConf).toBeDefined()
          expect(envConf.GITLAB_ENDPOINT).toBeDefined()
        })
      })
      it('and can verify that all values are available', () => {
        defaultOptions.errorOnMissing = true
        expect(() => {
          envConf = dotenvExtended.load(defaultOptions)
        }).toThrow()
      })
    })
  })
})
