# Style Guides

> Please follow the style guide applicable to the language or task.

## Table of Contents

<!-- ⛔️ AUTO-GENERATED-CONTENT:START (TOC:excludeText=Table of Contents) -->

- [1. Git Commit Messages](#1-git-commit-messages)
  * [1.1. Atomic commits](#11-atomic-commits)
  * [1.2. Commit message format](#12-commit-message-format)
  * [1.3. Revert](#13-revert)
  * [1.4. Type](#14-type)
  * [1.5. Subject](#15-subject)
  * [1.6. Body](#16-body)
  * [1.7. Footer](#17-footer)
  * [1.8. Examples](#18-examples)
- [2. Tests/Specs](#2-testsspecs)
- [3. Documentation](#3-documentation)
- [4. Source Code](#4-source-code)
  * [4.1. Lint](#41-lint)
  * [4.2. Languages](#42-languages)
    + [4.2.1. HTML/CSS](#421-htmlcss)
    + [4.2.2. Java](#422-java)
    + [4.2.3. JavaScript](#423-javascript)
    + [4.2.4. PHP](#424-php)
    + [4.2.5. Python](#425-python)
    + [4.2.6. Ruby](#426-ruby)
    + [4.2.7. Shell](#427-shell)
- [5. Attributions](#5-attributions)

<!-- ⛔️ AUTO-GENERATED-CONTENT:END -->

## 1. Git Commit Messages

<img alt="Git commit messages" height="48" width="48" valign="bottom" src="https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-commit.svg"> <img alt="Git commit messages" height="48" width="48" valign="bottom" src="https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/keyboard.svg">

### 1.1. Atomic commits

If possible, make [atomic commits ![external link][octicon-link-external]](https://en.wikipedia.org/wiki/Atomic_commit), which means:

- a commit should contain exactly one self-contained functional change
- a functional change should be contained in exactly one commit
- a commit should not create an inconsistent state (such as test errors, linting errors, partial fix, feature with documentation etc...)

A complex feature can be broken down into multiple commits as long as each one keep a consistent state and consist of a self-contained change.

### 1.2. Commit message format

Each commit message consists of a **header**, a **body** and a **footer**.

The header has a special format that includes a **type**, a **scope**, and a **subject**:

```commit
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory.

The **scope** of the header is optional.

The **footer** may contain an [issue closing pattern ![external link][octicon-link-external]](https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html).

### 1.3. Revert

If the commit reverts a previous commit, it should begin with `revert:`, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>`, where the hash is the SHA of the commit being reverted.

### 1.4. Type

The type must be one of the following:

| Type         | Description                                                                                                 |
| ------------ | ----------------------------------------------------------------------------------------------------------- |
| **build**    | Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)         |
| **chore**    | Administrative and management tasks                                                                         |
| **ci**       | Changes to the CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs) |
| **docs**     | Documentation only changes                                                                                  |
| **feat**     | A new feature                                                                                               |
| **fix**      | A bug fix                                                                                                   |
| **perf**     | A code change that improves performance                                                                     |
| **refactor** | A code change that neither fixes a bug nor adds a feature                                                   |
| **style**    | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)      |
| **test**     | Adding missing tests or correcting existing tests                                                           |

### 1.5. Subject

The subject contains succinct description of the change:

- Use the imperative, present tense: "change" not "changed" nor "changes"
- Don't capitalize first letter
- No dot (.) at the end
- Limit to 72 characters or fewer

### 1.6. Body

Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.
When only changing documentation, include `[ci skip]` in the commit body.

### 1.7. Footer

The footer should contain any information about **Breaking Changes** and is also the place to reference GitLab issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.

### 1.8. Examples

```commit
fix(pencil): stop graphite breaking when too much pressure applied
```

```commit
feat(pencil): add 'graphiteWidth' option

Fix #42
```

```commit
perf(pencil): remove graphiteWidth option

BREAKING CHANGE: The graphiteWidth option has been removed.

The default graphite width of 10mm is always used for performance reasons.
```

## 2. Tests/Specs

1. Include thoughtfully-worded, well-structured tests in a `__tests__` directory with each module.
2. Treat `describe` as a noun or situation.
3. Treat `it` as a statement about state or how an operation changes state.

> ![Tip!][octicon-light-bulb] **Use `npm run test:create <path/to/file.js>`**
>
> This will generate a Jest spec stub for your JavaScript.

## 3. Documentation

<img alt="Document with Markdown" height="80" width="80" src="https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/markdown.svg" valign="bottom"> Always write documentation in [Markdown](https://daringfireball.net/projects/markdown).

1. Use [GitLab Flavored Markdown (GFM)](https://docs.gitlab.com/ee/user/markdown.html#gitlab-flavored-markdown-gfm).
1. Refer to brands in [bold](https://help.github.com/articles/basic-writing-and-formatting-syntax/#styling-text) with proper capitalization, i.e.; **GitLab**, **npm**
1. Prefer [tables](https://help.GitLab.com/articles/organizing-information-with-tables) over [lists](https://help.GitLab.com/articles/basic-writing-and-formatting-syntax/#lists) when listing key values, i.e.; List of options with their description
1. Use [links](https://help.GitLab.com/articles/basic-writing-and-formatting-syntax/#links) when, the first you are referring to:
     - a concept described somewhere else in the documentation, i.e.; How to [contribute](CONTRIBUTING.md)
     - a third-party product/brand/service, i.e.; Integrate with [GitLab](https://GitLab.com)
     - an external concept or feature, i.e.; Create a [GitLab release](https://help.GitLab.com/articles/creating-releases)
     - a package or module, i.e.; The [`@seantrane/repo`](https://GitLab.com/seantrane/repo) module
1. Use [single backtick `code` quoting](https://help.GitLab.com/articles/basic-writing-and-formatting-syntax/#quoting-code) for:
     - commands inside sentences, i.e.; the `docker run` command
     - programming language keywords, i.e.; `function`, `async`, `String`
     - packages or modules, i.e.; The [`@seantrane/repo`](https://GitLab.com/seantrane/repo) module
1. Use [triple backtick `code` formatting](https://help.GitLab.com/articles/creating-and-highlighting-code-blocks) for:
     - Code examples
     - Configuration examples
     - Sequence of command lines
1. Reference methods and classes in markdown with the custom `{}` notation:
     - Reference classes with `{ClassName}`
     - Reference instance methods with `{ClassName#methodName}`
     - Reference class static methods with `{ClassName.methodName}`
     - Reference inner methods with `{ClassName~methodName}`

---

## 4. Source Code

To ensure consistency and quality throughout the source code, all code modification must have:

1. No [linting](#lint) errors
1. A [test](#tests) for every possible cases introduced by your code change
1. **100%** test coverage
1. [Valid commit message(s)](#git-commit-messages)
1. Documentation for new features
1. Updated documentation for modified features

### 4.1. Lint

Before pushing your code changes make sure there is no linting errors, i.e.; `npm run lint`.

> **![Tip!][octicon-light-bulb] TIP! `--fix` lint errors and warnings automatically**
>
> Many linting errors can be automatically fixed from the Termial by running:
> ```shell
> npm run lint:js --fix
> ```

---

<details><summary><img src="https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/package.svg" height="30" width="30" valign="bottom"> <strong>Toggle ESLint rules in use...</strong></summary>

<!-- AUTO-GENERATED-CONTENT:START (CODE:src=./.ci/eslint-rules&syntax=text&header=ESLint Rules) -->

<!-- The below code snippet is automatically added from ./.ci/eslint-rules -->
```text
ESLint Rules
current rules
395 rules found

accessor-pairs                                      https://eslint.org/docs/rules/accessor-pairs
array-bracket-newline                               https://eslint.org/docs/rules/array-bracket-newline
array-bracket-spacing                               https://eslint.org/docs/rules/array-bracket-spacing
array-callback-return                               https://eslint.org/docs/rules/array-callback-return
array-element-newline                               https://eslint.org/docs/rules/array-element-newline
arrow-body-style                                    https://eslint.org/docs/rules/arrow-body-style
arrow-parens                                        https://eslint.org/docs/rules/arrow-parens
arrow-spacing                                       https://eslint.org/docs/rules/arrow-spacing
block-scoped-var                                    https://eslint.org/docs/rules/block-scoped-var
block-spacing                                       https://eslint.org/docs/rules/block-spacing
brace-style                                         https://eslint.org/docs/rules/brace-style
callback-return                                     https://eslint.org/docs/rules/callback-return
camelcase                                           https://eslint.org/docs/rules/camelcase
capitalized-comments                                https://eslint.org/docs/rules/capitalized-comments
class-methods-use-this                              https://eslint.org/docs/rules/class-methods-use-this
comma-dangle                                        https://eslint.org/docs/rules/comma-dangle
comma-spacing                                       https://eslint.org/docs/rules/comma-spacing
comma-style                                         https://eslint.org/docs/rules/comma-style
complexity                                          https://eslint.org/docs/rules/complexity
computed-property-spacing                           https://eslint.org/docs/rules/computed-property-spacing
consistent-return                                   https://eslint.org/docs/rules/consistent-return
consistent-this                                     https://eslint.org/docs/rules/consistent-this
constructor-super                                   https://eslint.org/docs/rules/constructor-super
curly                                               https://eslint.org/docs/rules/curly
default-case                                        https://eslint.org/docs/rules/default-case
dot-location                                        https://eslint.org/docs/rules/dot-location
dot-notation                                        https://eslint.org/docs/rules/dot-notation
eol-last                                            https://eslint.org/docs/rules/eol-last
eqeqeq                                              https://eslint.org/docs/rules/eqeqeq
for-direction                                       https://eslint.org/docs/rules/for-direction
func-call-spacing                                   https://eslint.org/docs/rules/func-call-spacing
func-name-matching                                  https://eslint.org/docs/rules/func-name-matching
func-names                                          https://eslint.org/docs/rules/func-names
func-style                                          https://eslint.org/docs/rules/func-style
function-paren-newline                              https://eslint.org/docs/rules/function-paren-newline
generator-star                                      https://eslint.org/docs/rules/generator-star
generator-star-spacing                              https://eslint.org/docs/rules/generator-star-spacing
getter-return                                       https://eslint.org/docs/rules/getter-return
global-require                                      https://eslint.org/docs/rules/global-require
guard-for-in                                        https://eslint.org/docs/rules/guard-for-in
handle-callback-err                                 https://eslint.org/docs/rules/handle-callback-err
id-blacklist                                        https://eslint.org/docs/rules/id-blacklist
id-length                                           https://eslint.org/docs/rules/id-length
id-match                                            https://eslint.org/docs/rules/id-match
implicit-arrow-linebreak                            https://eslint.org/docs/rules/implicit-arrow-linebreak
import/default                                      https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/default.md
import/export                                       https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/export.md
import/first                                        https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/first.md
import/named                                        https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/named.md
import/namespace                                    https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/namespace.md
import/no-duplicates                                https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-duplicates.md
import/no-named-default                             https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-named-default.md
import/no-unresolved                                https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-unresolved.md
import/no-webpack-loader-syntax                     https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/no-webpack-loader-syntax.md
indent                                              https://eslint.org/docs/rules/indent
indent-legacy                                       https://eslint.org/docs/rules/indent-legacy
init-declarations                                   https://eslint.org/docs/rules/init-declarations
jsx-quotes                                          https://eslint.org/docs/rules/jsx-quotes
key-spacing                                         https://eslint.org/docs/rules/key-spacing
keyword-spacing                                     https://eslint.org/docs/rules/keyword-spacing
line-comment-position                               https://eslint.org/docs/rules/line-comment-position
linebreak-style                                     https://eslint.org/docs/rules/linebreak-style
lines-around-comment                                https://eslint.org/docs/rules/lines-around-comment
lines-around-directive                              https://eslint.org/docs/rules/lines-around-directive
lines-between-class-members                         https://eslint.org/docs/rules/lines-between-class-members
max-classes-per-file                                https://eslint.org/docs/rules/max-classes-per-file
max-depth                                           https://eslint.org/docs/rules/max-depth
max-len                                             https://eslint.org/docs/rules/max-len
max-lines                                           https://eslint.org/docs/rules/max-lines
max-lines-per-function                              https://eslint.org/docs/rules/max-lines-per-function
max-nested-callbacks                                https://eslint.org/docs/rules/max-nested-callbacks
max-params                                          https://eslint.org/docs/rules/max-params
max-statements                                      https://eslint.org/docs/rules/max-statements
max-statements-per-line                             https://eslint.org/docs/rules/max-statements-per-line
multiline-comment-style                             https://eslint.org/docs/rules/multiline-comment-style
multiline-ternary                                   https://eslint.org/docs/rules/multiline-ternary
new-cap                                             https://eslint.org/docs/rules/new-cap
new-parens                                          https://eslint.org/docs/rules/new-parens
newline-after-var                                   https://eslint.org/docs/rules/newline-after-var
newline-before-return                               https://eslint.org/docs/rules/newline-before-return
newline-per-chained-call                            https://eslint.org/docs/rules/newline-per-chained-call
no-alert                                            https://eslint.org/docs/rules/no-alert
no-array-constructor                                https://eslint.org/docs/rules/no-array-constructor
no-arrow-condition                                  https://eslint.org/docs/rules/no-arrow-condition
no-async-promise-executor                           https://eslint.org/docs/rules/no-async-promise-executor
no-await-in-loop                                    https://eslint.org/docs/rules/no-await-in-loop
no-bitwise                                          https://eslint.org/docs/rules/no-bitwise
no-buffer-constructor                               https://eslint.org/docs/rules/no-buffer-constructor
no-caller                                           https://eslint.org/docs/rules/no-caller
no-case-declarations                                https://eslint.org/docs/rules/no-case-declarations
no-catch-shadow                                     https://eslint.org/docs/rules/no-catch-shadow
no-class-assign                                     https://eslint.org/docs/rules/no-class-assign
no-comma-dangle                                     https://eslint.org/docs/rules/no-comma-dangle
no-compare-neg-zero                                 https://eslint.org/docs/rules/no-compare-neg-zero
no-cond-assign                                      https://eslint.org/docs/rules/no-cond-assign
no-confusing-arrow                                  https://eslint.org/docs/rules/no-confusing-arrow
no-console                                          https://eslint.org/docs/rules/no-console
no-const-assign                                     https://eslint.org/docs/rules/no-const-assign
no-constant-condition                               https://eslint.org/docs/rules/no-constant-condition
no-continue                                         https://eslint.org/docs/rules/no-continue
no-control-regex                                    https://eslint.org/docs/rules/no-control-regex
no-debugger                                         https://eslint.org/docs/rules/no-debugger
no-delete-var                                       https://eslint.org/docs/rules/no-delete-var
no-div-regex                                        https://eslint.org/docs/rules/no-div-regex
no-dupe-args                                        https://eslint.org/docs/rules/no-dupe-args
no-dupe-class-members                               https://eslint.org/docs/rules/no-dupe-class-members
no-dupe-keys                                        https://eslint.org/docs/rules/no-dupe-keys
no-duplicate-case                                   https://eslint.org/docs/rules/no-duplicate-case
no-duplicate-imports                                https://eslint.org/docs/rules/no-duplicate-imports
no-else-return                                      https://eslint.org/docs/rules/no-else-return
no-empty                                            https://eslint.org/docs/rules/no-empty
no-empty-character-class                            https://eslint.org/docs/rules/no-empty-character-class
no-empty-function                                   https://eslint.org/docs/rules/no-empty-function
no-empty-pattern                                    https://eslint.org/docs/rules/no-empty-pattern
no-eq-null                                          https://eslint.org/docs/rules/no-eq-null
no-eval                                             https://eslint.org/docs/rules/no-eval
no-ex-assign                                        https://eslint.org/docs/rules/no-ex-assign
no-extend-native                                    https://eslint.org/docs/rules/no-extend-native
no-extra-bind                                       https://eslint.org/docs/rules/no-extra-bind
no-extra-boolean-cast                               https://eslint.org/docs/rules/no-extra-boolean-cast
no-extra-label                                      https://eslint.org/docs/rules/no-extra-label
no-extra-parens                                     https://eslint.org/docs/rules/no-extra-parens
no-extra-semi                                       https://eslint.org/docs/rules/no-extra-semi
no-fallthrough                                      https://eslint.org/docs/rules/no-fallthrough
no-floating-decimal                                 https://eslint.org/docs/rules/no-floating-decimal
no-func-assign                                      https://eslint.org/docs/rules/no-func-assign
no-global-assign                                    https://eslint.org/docs/rules/no-global-assign
no-implicit-coercion                                https://eslint.org/docs/rules/no-implicit-coercion
no-implicit-globals                                 https://eslint.org/docs/rules/no-implicit-globals
no-implied-eval                                     https://eslint.org/docs/rules/no-implied-eval
no-inline-comments                                  https://eslint.org/docs/rules/no-inline-comments
no-inner-declarations                               https://eslint.org/docs/rules/no-inner-declarations
no-invalid-regexp                                   https://eslint.org/docs/rules/no-invalid-regexp
no-invalid-this                                     https://eslint.org/docs/rules/no-invalid-this
no-irregular-whitespace                             https://eslint.org/docs/rules/no-irregular-whitespace
no-iterator                                         https://eslint.org/docs/rules/no-iterator
no-label-var                                        https://eslint.org/docs/rules/no-label-var
no-labels                                           https://eslint.org/docs/rules/no-labels
no-lone-blocks                                      https://eslint.org/docs/rules/no-lone-blocks
no-lonely-if                                        https://eslint.org/docs/rules/no-lonely-if
no-loop-func                                        https://eslint.org/docs/rules/no-loop-func
no-magic-numbers                                    https://eslint.org/docs/rules/no-magic-numbers
no-misleading-character-class                       https://eslint.org/docs/rules/no-misleading-character-class
no-mixed-operators                                  https://eslint.org/docs/rules/no-mixed-operators
no-mixed-requires                                   https://eslint.org/docs/rules/no-mixed-requires
no-mixed-spaces-and-tabs                            https://eslint.org/docs/rules/no-mixed-spaces-and-tabs
no-multi-assign                                     https://eslint.org/docs/rules/no-multi-assign
no-multi-spaces                                     https://eslint.org/docs/rules/no-multi-spaces
no-multi-str                                        https://eslint.org/docs/rules/no-multi-str
no-multiple-empty-lines                             https://eslint.org/docs/rules/no-multiple-empty-lines
no-native-reassign                                  https://eslint.org/docs/rules/no-native-reassign
no-negated-condition                                https://eslint.org/docs/rules/no-negated-condition
no-negated-in-lhs                                   https://eslint.org/docs/rules/no-negated-in-lhs
no-nested-ternary                                   https://eslint.org/docs/rules/no-nested-ternary
no-new                                              https://eslint.org/docs/rules/no-new
no-new-func                                         https://eslint.org/docs/rules/no-new-func
no-new-object                                       https://eslint.org/docs/rules/no-new-object
no-new-require                                      https://eslint.org/docs/rules/no-new-require
no-new-symbol                                       https://eslint.org/docs/rules/no-new-symbol
no-new-wrappers                                     https://eslint.org/docs/rules/no-new-wrappers
no-obj-calls                                        https://eslint.org/docs/rules/no-obj-calls
no-octal                                            https://eslint.org/docs/rules/no-octal
no-octal-escape                                     https://eslint.org/docs/rules/no-octal-escape
no-param-reassign                                   https://eslint.org/docs/rules/no-param-reassign
no-path-concat                                      https://eslint.org/docs/rules/no-path-concat
no-plusplus                                         https://eslint.org/docs/rules/no-plusplus
no-process-env                                      https://eslint.org/docs/rules/no-process-env
no-process-exit                                     https://eslint.org/docs/rules/no-process-exit
no-proto                                            https://eslint.org/docs/rules/no-proto
no-prototype-builtins                               https://eslint.org/docs/rules/no-prototype-builtins
no-redeclare                                        https://eslint.org/docs/rules/no-redeclare
no-regex-spaces                                     https://eslint.org/docs/rules/no-regex-spaces
no-reserved-keys                                    https://eslint.org/docs/rules/no-reserved-keys
no-restricted-globals                               https://eslint.org/docs/rules/no-restricted-globals
no-restricted-imports                               https://eslint.org/docs/rules/no-restricted-imports
no-restricted-modules                               https://eslint.org/docs/rules/no-restricted-modules
no-restricted-properties                            https://eslint.org/docs/rules/no-restricted-properties
no-restricted-syntax                                https://eslint.org/docs/rules/no-restricted-syntax
no-return-assign                                    https://eslint.org/docs/rules/no-return-assign
no-return-await                                     https://eslint.org/docs/rules/no-return-await
no-script-url                                       https://eslint.org/docs/rules/no-script-url
no-self-assign                                      https://eslint.org/docs/rules/no-self-assign
no-self-compare                                     https://eslint.org/docs/rules/no-self-compare
no-sequences                                        https://eslint.org/docs/rules/no-sequences
no-shadow                                           https://eslint.org/docs/rules/no-shadow
no-shadow-restricted-names                          https://eslint.org/docs/rules/no-shadow-restricted-names
no-space-before-semi                                https://eslint.org/docs/rules/no-space-before-semi
no-spaced-func                                      https://eslint.org/docs/rules/no-spaced-func
no-sparse-arrays                                    https://eslint.org/docs/rules/no-sparse-arrays
no-sync                                             https://eslint.org/docs/rules/no-sync
no-tabs                                             https://eslint.org/docs/rules/no-tabs
no-template-curly-in-string                         https://eslint.org/docs/rules/no-template-curly-in-string
no-ternary                                          https://eslint.org/docs/rules/no-ternary
no-this-before-super                                https://eslint.org/docs/rules/no-this-before-super
no-throw-literal                                    https://eslint.org/docs/rules/no-throw-literal
no-trailing-spaces                                  https://eslint.org/docs/rules/no-trailing-spaces
no-undef                                            https://eslint.org/docs/rules/no-undef
no-undef-init                                       https://eslint.org/docs/rules/no-undef-init
no-undefined                                        https://eslint.org/docs/rules/no-undefined
no-underscore-dangle                                https://eslint.org/docs/rules/no-underscore-dangle
no-unexpected-multiline                             https://eslint.org/docs/rules/no-unexpected-multiline
no-unmodified-loop-condition                        https://eslint.org/docs/rules/no-unmodified-loop-condition
no-unneeded-ternary                                 https://eslint.org/docs/rules/no-unneeded-ternary
no-unreachable                                      https://eslint.org/docs/rules/no-unreachable
no-unsafe-finally                                   https://eslint.org/docs/rules/no-unsafe-finally
no-unsafe-innerhtml/no-unsafe-innerhtml             https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
no-unsafe-negation                                  https://eslint.org/docs/rules/no-unsafe-negation
no-unsanitized/method                               https://github.com/mozilla/eslint-plugin-no-unsanitized/blob/master/docs/rules/method.md
no-unsanitized/property                             https://github.com/mozilla/eslint-plugin-no-unsanitized/blob/master/docs/rules/property.md
no-unused-expressions                               https://eslint.org/docs/rules/no-unused-expressions
no-unused-labels                                    https://eslint.org/docs/rules/no-unused-labels
no-unused-vars                                      https://eslint.org/docs/rules/no-unused-vars
no-use-before-define                                https://eslint.org/docs/rules/no-use-before-define
no-useless-call                                     https://eslint.org/docs/rules/no-useless-call
no-useless-catch                                    https://eslint.org/docs/rules/no-useless-catch
no-useless-computed-key                             https://eslint.org/docs/rules/no-useless-computed-key
no-useless-concat                                   https://eslint.org/docs/rules/no-useless-concat
no-useless-constructor                              https://eslint.org/docs/rules/no-useless-constructor
no-useless-escape                                   https://eslint.org/docs/rules/no-useless-escape
no-useless-rename                                   https://eslint.org/docs/rules/no-useless-rename
no-useless-return                                   https://eslint.org/docs/rules/no-useless-return
no-var                                              https://eslint.org/docs/rules/no-var
no-void                                             https://eslint.org/docs/rules/no-void
no-warning-comments                                 https://eslint.org/docs/rules/no-warning-comments
no-whitespace-before-property                       https://eslint.org/docs/rules/no-whitespace-before-property
no-with                                             https://eslint.org/docs/rules/no-with
no-wrap-func                                        https://eslint.org/docs/rules/no-wrap-func
node/exports-style                                  https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/exports-style.md
node/no-deprecated-api                              https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/no-deprecated-api.md
node/no-extraneous-import                           https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/no-extraneous-import.md
node/no-extraneous-require                          https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/no-extraneous-require.md
node/no-missing-import                              https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/no-missing-import.md
node/no-missing-require                             https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/no-missing-require.md
node/no-unpublished-bin                             https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/no-unpublished-bin.md
node/no-unpublished-import                          https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/no-unpublished-import.md
node/no-unpublished-require                         https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/no-unpublished-require.md
node/no-unsupported-features/es-syntax              https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/no-unsupported-features.md
node/process-exit-as-throw                          https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/process-exit-as-throw.md
node/shebang                                        https://github.com/mysticatea/eslint-plugin-node/blob/master/docs/rules/shebang.md
nonblock-statement-body-position                    https://eslint.org/docs/rules/nonblock-statement-body-position
object-curly-newline                                https://eslint.org/docs/rules/object-curly-newline
object-curly-spacing                                https://eslint.org/docs/rules/object-curly-spacing
object-property-newline                             https://eslint.org/docs/rules/object-property-newline
object-shorthand                                    https://eslint.org/docs/rules/object-shorthand
one-var                                             https://eslint.org/docs/rules/one-var
one-var-declaration-per-line                        https://eslint.org/docs/rules/one-var-declaration-per-line
operator-assignment                                 https://eslint.org/docs/rules/operator-assignment
operator-linebreak                                  https://eslint.org/docs/rules/operator-linebreak
padded-blocks                                       https://eslint.org/docs/rules/padded-blocks
padding-line-between-statements                     https://eslint.org/docs/rules/padding-line-between-statements
prefer-arrow-callback                               https://eslint.org/docs/rules/prefer-arrow-callback
prefer-const                                        https://eslint.org/docs/rules/prefer-const
prefer-destructuring                                https://eslint.org/docs/rules/prefer-destructuring
prefer-numeric-literals                             https://eslint.org/docs/rules/prefer-numeric-literals
prefer-object-spread                                https://eslint.org/docs/rules/prefer-object-spread
prefer-promise-reject-errors                        https://eslint.org/docs/rules/prefer-promise-reject-errors
prefer-reflect                                      https://eslint.org/docs/rules/prefer-reflect
prefer-rest-params                                  https://eslint.org/docs/rules/prefer-rest-params
prefer-spread                                       https://eslint.org/docs/rules/prefer-spread
prefer-template                                     https://eslint.org/docs/rules/prefer-template
promise/param-names                                 https://github.com/xjamundx/eslint-plugin-promise/blob/master/docs/rules/param-names.md
quote-props                                         https://eslint.org/docs/rules/quote-props
quotes                                              https://eslint.org/docs/rules/quotes
radix                                               https://eslint.org/docs/rules/radix
require-atomic-updates                              https://eslint.org/docs/rules/require-atomic-updates
require-await                                       https://eslint.org/docs/rules/require-await
require-jsdoc                                       https://eslint.org/docs/rules/require-jsdoc
require-unicode-regexp                              https://eslint.org/docs/rules/require-unicode-regexp
require-yield                                       https://eslint.org/docs/rules/require-yield
rest-spread-spacing                                 https://eslint.org/docs/rules/rest-spread-spacing
scanjs-rules/accidental_assignment                  https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/assign_to_hostname                     https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/assign_to_href                         https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/assign_to_location                     https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/assign_to_onmessage                    https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/assign_to_pathname                     https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/assign_to_protocol                     https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/assign_to_search                       https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/assign_to_src                          https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_Function                          https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_addEventListener                  https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_addEventListener_deviceproximity  https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_addEventListener_message          https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_connect                           https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_eval                              https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_execScript                        https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_hide                              https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_open_remote=true                  https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_parseFromString                   https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_setImmediate                      https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_setInterval                       https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/call_setTimeout                        https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/identifier_indexedDB                   https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/identifier_localStorage                https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/identifier_sessionStorage              https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/new_Function                           https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/property_addIdleObserver               https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/property_createContextualFragment      https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/property_crypto                        https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/property_geolocation                   https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/property_getUserMedia                  https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/property_indexedDB                     https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/property_localStorage                  https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/property_mgmt                          https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
scanjs-rules/property_sessionStorage                https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-buffer-noassert                     https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-child-process                       https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-disable-mustache-escape             https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-eval-with-expression                https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-new-buffer                          https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-no-csrf-before-method-override      https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-non-literal-fs-filename             https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-non-literal-regexp                  https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-non-literal-require                 https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-object-injection                    https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-possible-timing-attacks             https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-pseudoRandomBytes                   https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
security/detect-unsafe-regex                        https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
semi                                                https://eslint.org/docs/rules/semi
semi-spacing                                        https://eslint.org/docs/rules/semi-spacing
semi-style                                          https://eslint.org/docs/rules/semi-style
sonarjs/cognitive-complexity                        https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/max-switch-cases                            https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-all-duplicated-branches                  https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-duplicate-string                         https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-duplicated-branches                      https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-element-overwrite                        https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-extra-arguments                          https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-identical-conditions                     https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-identical-expressions                    https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-identical-functions                      https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-inverted-boolean-check                   https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-one-iteration-loop                       https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-redundant-boolean                        https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-small-switch                             https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-use-of-empty-return-value                https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/no-useless-catch                            https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/prefer-immediate-return                     https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/prefer-object-literal                       https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/prefer-single-boolean-return                https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sonarjs/prefer-while                                https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
sort-imports                                        https://eslint.org/docs/rules/sort-imports
sort-keys                                           https://eslint.org/docs/rules/sort-keys
sort-vars                                           https://eslint.org/docs/rules/sort-vars
space-after-function-name                           https://eslint.org/docs/rules/space-after-function-name
space-after-keywords                                https://eslint.org/docs/rules/space-after-keywords
space-before-blocks                                 https://eslint.org/docs/rules/space-before-blocks
space-before-function-paren                         https://eslint.org/docs/rules/space-before-function-paren
space-before-function-parentheses                   https://eslint.org/docs/rules/space-before-function-parentheses
space-before-keywords                               https://eslint.org/docs/rules/space-before-keywords
space-in-brackets                                   https://eslint.org/docs/rules/space-in-brackets
space-in-parens                                     https://eslint.org/docs/rules/space-in-parens
space-infix-ops                                     https://eslint.org/docs/rules/space-infix-ops
space-return-throw-case                             https://eslint.org/docs/rules/space-return-throw-case
space-unary-ops                                     https://eslint.org/docs/rules/space-unary-ops
space-unary-word-ops                                https://eslint.org/docs/rules/space-unary-word-ops
spaced-comment                                      https://eslint.org/docs/rules/spaced-comment
standard/array-bracket-even-spacing                 https://github.com/xjamundx/eslint-plugin-standard#rules-explanations
standard/computed-property-even-spacing             https://github.com/xjamundx/eslint-plugin-standard#rules-explanations
standard/no-callback-literal                        https://github.com/xjamundx/eslint-plugin-standard#rules-explanations
standard/object-curly-even-spacing                  https://github.com/xjamundx/eslint-plugin-standard#rules-explanations
strict                                              https://eslint.org/docs/rules/strict
switch-colon-spacing                                https://eslint.org/docs/rules/switch-colon-spacing
symbol-description                                  https://eslint.org/docs/rules/symbol-description
template-curly-spacing                              https://eslint.org/docs/rules/template-curly-spacing
template-tag-spacing                                https://eslint.org/docs/rules/template-tag-spacing
unicode-bom                                         https://eslint.org/docs/rules/unicode-bom
unicorn/catch-error-name                            https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/catch-error-name.md
unicorn/custom-error-definition                     https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/custom-error-definition.md
unicorn/escape-case                                 https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/escape-case.md
unicorn/explicit-length-check                       https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/explicit-length-check.md
unicorn/filename-case                               https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/filename-case.md
unicorn/import-index                                https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/import-index.md
unicorn/new-for-builtins                            https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/new-for-builtins.md
unicorn/no-abusive-eslint-disable                   https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/no-abusive-eslint-disable.md
unicorn/no-array-instanceof                         https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/no-array-instanceof.md
unicorn/no-fn-reference-in-iterator                 https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/no-fn-reference-in-iterator.md
unicorn/no-hex-escape                               https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/no-hex-escape.md
unicorn/no-new-buffer                               https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/no-new-buffer.md
unicorn/no-process-exit                             https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/no-process-exit.md
unicorn/number-literal-case                         https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/number-literal-case.md
unicorn/prefer-starts-ends-with                     https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/prefer-starts-ends-with.md
unicorn/prefer-type-error                           https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/prefer-type-error.md
unicorn/regex-shorthand                             https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/regex-shorthand.md
unicorn/throw-new-error                             https://github.com/sindresorhus/eslint-plugin-unicorn/blob/master/docs/rules/throw-new-error.md
use-isnan                                           https://eslint.org/docs/rules/use-isnan
valid-jsdoc                                         https://eslint.org/docs/rules/valid-jsdoc
valid-typeof                                        https://eslint.org/docs/rules/valid-typeof
vars-on-top                                         https://eslint.org/docs/rules/vars-on-top
wrap-iife                                           https://eslint.org/docs/rules/wrap-iife
wrap-regex                                          https://eslint.org/docs/rules/wrap-regex
xss/no-location-href-assign                         https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
xss/no-mixed-html                                   https://github.com/jfmengels/eslint-rule-documentation/blob/master/contributing.md
yield-star-spacing                                  https://eslint.org/docs/rules/yield-star-spacing
yoda                                                https://eslint.org/docs/rules/yoda
```

<!-- AUTO-GENERATED-CONTENT:END -->

</details>

---

### 4.2. Languages

#### 4.2.1. HTML/CSS

All HTML/CSS _SHOULD_ adhere to [Google's HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html).

#### 4.2.2. Java

All Java syntax _MUST_ adhere to [Google's Java Style Guide](https://google.github.io/styleguide/javaguide.html).

#### 4.2.3. JavaScript

1. All JavaScript syntax should adhere to [JavaScript Standard Style](http://standardjs.com/).
2. All Angular syntax must adhere to [The Angular Style Guide](https://angular.io/guide/styleguide).
3. Run `npm run lint:js` to evaluate with [`ESLint`](https://eslint.org/).

#### 4.2.4. PHP

All PHP syntax must adhere to [PSR-2 Coding Style Guide](http://www.php-fig.org/psr/psr-2/).

#### 4.2.5. Python

1. All Python syntax must adhere to [Google's Python Style Guide](https://google.github.io/styleguide/pyguide.html).
2. Run [`pylint`](https://www.pylint.org/) over your code.

#### 4.2.6. Ruby

1. All Ruby syntax must adhere to [The Ruby Style Guide](https://GitLab.com/bbatsov/ruby-style-guide).
1. Run [`rubocop`](https://GitLab.com/bbatsov/rubocop) over your code.

#### 4.2.7. Shell

1. All Shell syntax must adhere to [Google's Shell Style Guide](https://google.github.io/styleguide/shell.xml).
1. Run [ShellCheck](http://www.shellcheck.net/) over your code.

---

## 5. Attributions

This document borrows heavily from _seantrane/yo-repo._ (2019). _GitLab_. Retrieved 29 January 2019, from <https://GitLab.com/seantrane/yo-repo/blob/master/src/contributing/templates/STYLE_GUIDES.md>

<!-- ⛔️ Do not remove this comment or anything below it ⛔️  -->

[octicon-alert]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/alert.svg

[octicon-arrow-down]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-down.svg

[octicon-arrow-left]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-left.svg

[octicon-arrow-right]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-right.svg

[octicon-arrow-small-down]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-small-down.svg

[octicon-arrow-small-left]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-small-left.svg

[octicon-arrow-small-right]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-small-right.svg

[octicon-arrow-small-up]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-small-up.svg

[octicon-arrow-up]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-up.svg

[octicon-beaker]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/beaker.svg

[octicon-bell]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/bell.svg

[octicon-bold]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/bold.svg

[octicon-book]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/book.svg

[octicon-bookmark]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/bookmark.svg

[octicon-briefcase]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/briefcase.svg

[octicon-broadcast]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/broadcast.svg

[octicon-browser]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/browser.svg

[octicon-bug]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/bug.svg

[octicon-calendar]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/calendar.svg

[octicon-check]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/check.svg

[octicon-checklist]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/checklist.svg

[octicon-chevron-down]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/chevron-down.svg

[octicon-chevron-left]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/chevron-left.svg

[octicon-chevron-right]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/chevron-right.svg

[octicon-chevron-up]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/chevron-up.svg

[octicon-circle-slash]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/circle-slash.svg

[octicon-circuit-board]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/circuit-board.svg

[octicon-clippy]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/clippy.svg

[octicon-clock]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/clock.svg

[octicon-cloud-download]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/cloud-download.svg

[octicon-cloud-upload]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/cloud-upload.svg

[octicon-code]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/code.svg

[octicon-comment-discussion]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/comment-discussion.svg

[octicon-comment]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/comment.svg

[octicon-credit-card]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/credit-card.svg

[octicon-dash]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/dash.svg

[octicon-dashboard]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/dashboard.svg

[octicon-database]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/database.svg

[octicon-desktop-download]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/desktop-download.svg

[octicon-device-camera-video]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/device-camera-video.svg

[octicon-device-camera]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/device-camera.svg

[octicon-device-desktop]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/device-desktop.svg

[octicon-device-mobile]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/device-mobile.svg

[octicon-diff-added]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff-added.svg

[octicon-diff-ignored]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff-ignored.svg

[octicon-diff-modified]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff-modified.svg

[octicon-diff-removed]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff-removed.svg

[octicon-diff-renamed]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff-renamed.svg

[octicon-diff]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff.svg

[octicon-ellipses]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/ellipses.svg

[octicon-ellipsis]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/ellipsis.svg

[octicon-eye]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/eye.svg

[octicon-file-binary]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-binary.svg

[octicon-file-code]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-code.svg

[octicon-file-directory]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-directory.svg

[octicon-file-media]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-media.svg

[octicon-file-pdf]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-pdf.svg

[octicon-file-submodule]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-submodule.svg

[octicon-file-symlink-directory]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-symlink-directory.svg

[octicon-file-symlink-file]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-symlink-file.svg

[octicon-file-text]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-text.svg

[octicon-file-zip]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-zip.svg

[octicon-file]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file.svg

[octicon-flame]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/flame.svg

[octicon-fold]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/fold.svg

[octicon-gear]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/gear.svg

[octicon-gift]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/gift.svg

[octicon-gist-secret]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/gist-secret.svg

[octicon-gist]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/gist.svg

[octicon-git-branch]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-branch.svg

[octicon-git-commit]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-commit.svg

[octicon-git-compare]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-compare.svg

[octicon-git-merge]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-merge.svg

[octicon-git-pull-request]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-pull-request.svg

[octicon-globe]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/globe.svg

[octicon-grabber]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/grabber.svg

[octicon-graph]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/graph.svg

[octicon-heart]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/heart.svg

[octicon-history]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/history.svg

[octicon-home]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/home.svg

[octicon-horizontal-rule]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/horizontal-rule.svg

[octicon-hubot]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/hubot.svg

[octicon-inbox]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/inbox.svg

[octicon-info]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/info.svg

[octicon-issue-closed]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/issue-closed.svg

[octicon-issue-opened]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/issue-opened.svg

[octicon-issue-reopened]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/issue-reopened.svg

[octicon-italic]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/italic.svg

[octicon-jersey]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/jersey.svg

[octicon-key]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/key.svg

[octicon-keyboard]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/keyboard.svg

[octicon-law]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/law.svg

[octicon-light-bulb]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/light-bulb.svg

[octicon-link-external]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/link-external.svg

[octicon-link]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/link.svg

[octicon-list-ordered]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/list-ordered.svg

[octicon-list-unordered]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/list-unordered.svg

[octicon-location]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/location.svg

[octicon-lock]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/lock.svg

[octicon-logo-gist]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/logo-gist.svg

[octicon-logo-GitLab]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/logo-GitLab.svg

[octicon-mail-read]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mail-read.svg

[octicon-mail-reply]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mail-reply.svg

[octicon-mail]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mail.svg

[octicon-mark-GitLab]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mark-GitLab.svg

[octicon-markdown]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/markdown.svg

[octicon-megaphone]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/megaphone.svg

[octicon-mention]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mention.svg

[octicon-milestone]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/milestone.svg

[octicon-mirror]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mirror.svg

[octicon-mortar-board]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mortar-board.svg

[octicon-mute]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mute.svg

[octicon-no-newline]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/no-newline.svg

[octicon-octoface]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/octoface.svg

[octicon-organization]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/organization.svg

[octicon-package]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/package.svg

[octicon-paintcan]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/paintcan.svg

[octicon-pencil]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/pencil.svg

[octicon-person]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/person.svg

[octicon-pin]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/pin.svg

[octicon-plug]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/plug.svg

[octicon-plus-small]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/plus-small.svg

[octicon-plus]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/plus.svg

[octicon-primitive-dot]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/primitive-dot.svg

[octicon-primitive-square]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/primitive-square.svg

[octicon-pulse]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/pulse.svg

[octicon-question]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/question.svg

[octicon-quote]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/quote.svg

[octicon-radio-tower]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/radio-tower.svg

[octicon-reply]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/reply.svg

[octicon-repo-clone]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo-clone.svg

[octicon-repo-force-push]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo-force-push.svg

[octicon-repo-forked]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo-forked.svg

[octicon-repo-pull]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo-pull.svg

[octicon-repo-push]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo-push.svg

[octicon-repo]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo.svg

[octicon-rocket]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/rocket.svg

[octicon-rss]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/rss.svg

[octicon-ruby]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/ruby.svg

[octicon-search]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/search.svg

[octicon-server]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/server.svg

[octicon-settings]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/settings.svg

[octicon-shield]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/shield.svg

[octicon-sign-in]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/sign-in.svg

[octicon-sign-out]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/sign-out.svg

[octicon-smiley]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/smiley.svg

[octicon-squirrel]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/squirrel.svg

[octicon-star]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/star.svg

[octicon-stop]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/stop.svg

[octicon-sync]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/sync.svg

[octicon-tag]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/tag.svg

[octicon-tasklist]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/tasklist.svg

[octicon-telescope]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/telescope.svg

[octicon-terminal]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/terminal.svg

[octicon-text-size]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/text-size.svg

[octicon-three-bars]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/three-bars.svg

[octicon-thumbsdown]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/thumbsdown.svg

[octicon-thumbsup]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/thumbsup.svg

[octicon-tools]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/tools.svg

[octicon-trashcan]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/trashcan.svg

[octicon-triangle-down]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/triangle-down.svg

[octicon-triangle-left]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/triangle-left.svg

[octicon-triangle-right]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/triangle-right.svg

[octicon-triangle-up]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/triangle-up.svg

[octicon-unfold]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/unfold.svg

[octicon-unmute]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/unmute.svg

[octicon-unverified]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/unverified.svg

[octicon-verified]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/verified.svg

[octicon-versions]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/versions.svg

[octicon-watch]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/watch.svg

[octicon-x]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/x.svg
