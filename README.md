# signatures _(@data-leakage-protection/signatures)_

![logo](docs/img/logos/logo.png)

> ![Product summary][octicon-book] Identify confidential and sensitive info in source code repositories by data-loss "signatures".

**@data-leakage-protection/signatures** is a Node.js
[module ![offsite web page][octicon-link-external]][npmjs-about-modules-doc]
for storing and accessing to data-leakage detection definitions.
We call the data structure that represents a data-leakage detection
defintion a "signature." We store a community-tested list of signatures in a
file called `signatures.json`.

## Table of Contents

<!-- ⛔️ AUTO-GENERATED-CONTENT:START (TOC) -->
- [1. Security](#1-security)
- [2. Install](#2-install)
- [3. Usage](#3-usage)
- [4. API](#4-api)
  * [4.1. `@data-leakage-protection/signatures.Signature`](#41-data-leakage-protectionsignaturessignature)
  * [4.2. `@data-leakage-protection/signatures.Signature.prototype.match`](#42-data-leakage-protectionsignaturessignatureprototypematch)
- [5. Accessing signatures with other tools and programming languages](#5-accessing-signatures-with-other-tools-and-programming-languages)
- [6. Maintainers](#6-maintainers)
- [7. Contributions](#7-contributions)
  * [7.1. Adding a Signature](#71-adding-a-signature)
  * [7.2. Editing a Signature](#72-editing-a-signature)
  * [7.3. Removing a Signature](#73-removing-a-signature)
- [8. License](#8-license)
- [9. References and Attributions](#9-references-and-attributions)
<!-- ⛔️ AUTO-GENERATED-CONTENT:END -->

## 1. Security

> ![citation][octicon-quote] Data leakage is the unauthorized transmission of
> data from within an organization to an external destination or recipient.[^1]

One of the most common forms of data-loss (aka, "data leakage") happens when
developers (inadvertently) commit and push passwords, access-tokens, and sensitive data to a
source-control management system (like Git). Consequently, confidential information "leaks" into search results and commit history.

The <samp>signatures.json</samp> contains a growing list of definitions to help you detect secrets in your source code repositories.

<!-- START:(@data-leakage-protection/signatures table) -->

|     | Signature | Detected in |
|----:|-----------|-------------|
| 1 | **.asc file extension**<br><small>_Potential cryptographic key bundle_</small> | <samp>extension</samp> |
| 2 | **.p12 file extension**<br><small>_PKCS#12 (.p12): potential cryptographic key bundle_</small> | <samp>extension</samp> |
| 3 | **.pem file extension**<br><small>_Potential cryptographic private key_</small> | <samp>extension</samp> |
| 4 | **.pfx file extension**<br><small>_PKCS#12 (.pfx): Potential cryptographic key bundle_</small> | <samp>extension</samp> |
| 5 | **.pkcs12 file extension**<br><small>_PKCS#12 (.pkcs12): Potential cryptographic key bundle_</small> | <samp>extension</samp> |
| 6 | **1Password password manager database file**<br><small>_Feed it to Hashcat and see if you're lucky_</small> | <samp>extension</samp> |
| 7 | **AWS API Key**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 8 | **AWS CLI credentials file**<br><small>__</small> | <samp>path</samp> |
| 9 | **Apache htpasswd file**<br><small>__</small> | <samp>filename</samp> |
| 10 | **Apple Keychain database file**<br><small>__</small> | <samp>extension</samp> |
| 11 | **Azure service configuration schema file**<br><small>__</small> | <samp>extension</samp> |
| 12 | **Carrierwave configuration file**<br><small>_Can contain credentials for cloud storage systems such as Amazon S3 and Google Storage_</small> | <samp>filename</samp> |
| 13 | **Chef Knife configuration file**<br><small>_Can contain references to Chef servers_</small> | <samp>filename</samp> |
| 14 | **Chef private key**<br><small>_Can be used to authenticate against Chef servers_</small> | <samp>path</samp> |
| 15 | **Configuration file for auto-login process**<br><small>_Can contain username and password_</small> | <samp>filename</samp> |
| 16 | **Contains word: credential**<br><small>__</small> | <samp>path</samp> |
| 17 | **Contains word: password**<br><small>__</small> | <samp>path</samp> |
| 18 | **DBeaver SQL database manager configuration file**<br><small>__</small> | <samp>filename</samp> |
| 19 | **Day One journal file**<br><small>_Now it's getting creepy..._</small> | <samp>extension</samp> |
| 20 | **DigitalOcean doctl command-line client configuration file**<br><small>_Contains DigitalOcean API key and other information_</small> | <samp>path</samp> |
| 21 | **Django configuration file**<br><small>_Can contain database credentials, cloud storage system credentials, and other secrets_</small> | <samp>filename</samp> |
| 22 | **Docker configuration file**<br><small>_Can contain credentials for public or private Docker registries_</small> | <samp>filename</samp> |
| 23 | **Environment configuration file**<br><small>__</small> | <samp>filename</samp> |
| 24 | **Facebook Oauth**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 25 | **FileZilla FTP configuration file**<br><small>_Can contain credentials for FTP servers_</small> | <samp>filename</samp> |
| 26 | **FileZilla FTP recent servers file**<br><small>_Can contain credentials for FTP servers_</small> | <samp>filename</samp> |
| 27 | **GNOME Keyring database file**<br><small>__</small> | <samp>extension</samp> |
| 28 | **Generic API Key**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 29 | **Generic Secret**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 30 | **Git configuration file**<br><small>__</small> | <samp>filename</samp> |
| 31 | **GitHub**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 32 | **GitHub Hub command-line client configuration file**<br><small>_Can contain GitHub API access token_</small> | <samp>path</samp> |
| 33 | **GnuCash database file**<br><small>__</small> | <samp>extension</samp> |
| 34 | **Google (GCP) Service-account**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 35 | **Google Oauth**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 36 | **Heroku API Key**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 37 | **Hexchat/XChat IRC client server list configuration file**<br><small>__</small> | <samp>path</samp> |
| 38 | **Irssi IRC client configuration file**<br><small>__</small> | <samp>path</samp> |
| 39 | **Java keystore file**<br><small>__</small> | <samp>extension</samp> |
| 40 | **Jenkins publish over SSH plugin file**<br><small>__</small> | <samp>filename</samp> |
| 41 | **KDE Wallet Manager database file**<br><small>__</small> | <samp>extension</samp> |
| 42 | **KeePass password manager database file**<br><small>_Feed it to Hashcat and see if you're lucky_</small> | <samp>extension</samp> |
| 43 | **Little Snitch firewall configuration file**<br><small>_Contains traffic rules for applications_</small> | <samp>filename</samp> |
| 44 | **Log file**<br><small>_Log files can contain secret HTTP endpoints, session IDs, API keys and other goodies_</small> | <samp>extension</samp> |
| 45 | **Microsoft BitLocker Trusted Platform Module password file**<br><small>__</small> | <samp>extension</samp> |
| 46 | **Microsoft BitLocker recovery key file**<br><small>__</small> | <samp>extension</samp> |
| 47 | **Microsoft SQL database file**<br><small>__</small> | <samp>extension</samp> |
| 48 | **Microsoft SQL server compact database file**<br><small>__</small> | <samp>extension</samp> |
| 49 | **Mutt e-mail client configuration file**<br><small>__</small> | <samp>filename</samp> |
| 50 | **MySQL client command history file**<br><small>__</small> | <samp>filename</samp> |
| 51 | **NPM configuration file**<br><small>_Can contain credentials for NPM registries_</small> | <samp>filename</samp> |
| 52 | **Network traffic capture file**<br><small>__</small> | <samp>extension</samp> |
| 53 | **OmniAuth configuration file**<br><small>_The OmniAuth configuration file can contain client application secrets_</small> | <samp>filename</samp> |
| 54 | **OpenVPN client configuration file**<br><small>__</small> | <samp>extension</samp> |
| 55 | **PGP private key block**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 56 | **PHP configuration file**<br><small>__</small> | <samp>filename</samp> |
| 57 | **Password Safe database file**<br><small>__</small> | <samp>extension</samp> |
| 58 | **Password in URL**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 59 | **Pidgin OTR private key**<br><small>__</small> | <samp>filename</samp> |
| 60 | **Pidgin chat client account configuration file**<br><small>__</small> | <samp>path</samp> |
| 61 | **PostgreSQL client command history file**<br><small>__</small> | <samp>filename</samp> |
| 62 | **PostgreSQL password file**<br><small>__</small> | <samp>filename</samp> |
| 63 | **Potential Jenkins credentials file**<br><small>__</small> | <samp>filename</samp> |
| 64 | **Potential Linux passwd file**<br><small>_Contains system user information_</small> | <samp>path</samp> |
| 65 | **Potential Linux shadow file**<br><small>_Contains hashed passwords for system users_</small> | <samp>path</samp> |
| 66 | **Potential MediaWiki configuration file**<br><small>__</small> | <samp>filename</samp> |
| 67 | **Potential Ruby On Rails database configuration file**<br><small>_Can contain database credentials_</small> | <samp>filename</samp> |
| 68 | **Potential cryptographic private key**<br><small>__</small> | <samp>extension</samp> |
| 69 | **Potential jrnl journal file**<br><small>_Now it's getting creepy..._</small> | <samp>filename</samp> |
| 70 | **Private SSH key**<br><small>__rsa_</small> | <samp>filename</samp> |
| 71 | **Private SSH key**<br><small>__dsa_</small> | <samp>filename</samp> |
| 72 | **Private SSH key**<br><small>__ed25519_</small> | <samp>filename</samp> |
| 73 | **Private SSH key**<br><small>__ecdsa_</small> | <samp>filename</samp> |
| 74 | **RSA private key**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 75 | **Recon-ng web reconnaissance framework API key database**<br><small>__</small> | <samp>path</samp> |
| 76 | **Remote Desktop connection file**<br><small>__</small> | <samp>extension</samp> |
| 77 | **Robomongo MongoDB manager configuration file**<br><small>_Can contain credentials for MongoDB databases_</small> | <samp>filename</samp> |
| 78 | **Ruby IRB console history file**<br><small>__</small> | <samp>filename</samp> |
| 79 | **Ruby On Rails secret token configuration file**<br><small>_If the Rails secret token is known, it can allow for remote code execution (http://www.exploit-db.com/exploits/27527/)_</small> | <samp>filename</samp> |
| 80 | **Rubygems credentials file**<br><small>_Can contain API key for a rubygems.org account_</small> | <samp>path</samp> |
| 81 | **S3cmd configuration file**<br><small>__</small> | <samp>filename</samp> |
| 82 | **SFTP connection configuration file**<br><small>__</small> | <samp>filename</samp> |
| 83 | **SQL dump file**<br><small>__</small> | <samp>extension</samp> |
| 84 | **SQLite database file**<br><small>__</small> | <samp>extension</samp> |
| 85 | **SSH (DSA) private key**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 86 | **SSH (EC) private key**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 87 | **SSH (OPENSSH) private key**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 88 | **SSH configuration file**<br><small>__</small> | <samp>path</samp> |
| 89 | **Sequel Pro MySQL database manager bookmark file**<br><small>__</small> | <samp>filename</samp> |
| 90 | **Shell command alias configuration file**<br><small>_Shell configuration files can contain passwords, API keys, hostnames and other goodies_</small> | <samp>filename</samp> |
| 91 | **Shell command history file**<br><small>__</small> | <samp>filename</samp> |
| 92 | **Shell configuration file**<br><small>_(.exports): Shell configuration files can contain passwords, API keys, hostnames and other goodies_</small> | <samp>filename</samp> |
| 93 | **Shell configuration file**<br><small>_(.functions): Shell configuration files can contain passwords, API keys, hostnames and other goodies_</small> | <samp>filename</samp> |
| 94 | **Shell configuration file**<br><small>_(.extra): Shell configuration files can contain passwords, API keys, hostnames and other goodies_</small> | <samp>filename</samp> |
| 95 | **Shell configuration file**<br><small>_(bash, zsh, csh): Shell configuration files can contain passwords, API keys, hostnames and other goodies_</small> | <samp>filename</samp> |
| 96 | **Shell profile configuration file**<br><small>_(profile): Shell configuration files can contain passwords, API keys, hostnames and other goodies_</small> | <samp>filename</samp> |
| 97 | **Slack Token**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 98 | **Slack Webhook**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 99 | **T command-line Twitter client configuration file**<br><small>__</small> | <samp>filename</samp> |
| 100 | **Terraform variable config file**<br><small>_Can contain credentials for terraform providers_</small> | <samp>filename</samp> |
| 101 | **Tugboat DigitalOcean management tool configuration**<br><small>__</small> | <samp>filename</samp> |
| 102 | **Tunnelblick VPN configuration file**<br><small>__</small> | <samp>extension</samp> |
| 103 | **Twilio API Key**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 104 | **Twitter Oauth**<br><small>_Imported from [rules.json](https://gitlab.verizon.com/V507783/trufflehog_test/blob/da6074a071a54375438cc1dcef7811a72e9a8d5d/rules.json)._</small> | <samp>contents</samp> |
| 105 | **Ventrilo server configuration file**<br><small>_Can contain passwords_</small> | <samp>filename</samp> |
| 106 | **Windows BitLocker full volume encrypted data file**<br><small>__</small> | <samp>extension</samp> |
| 107 | **cPanel backup ProFTPd credentials file**<br><small>_Contains usernames and password hashes for FTP accounts_</small> | <samp>filename</samp> |
| 108 | **git-credential-store helper credentials file**<br><small>__</small> | <samp>filename</samp> |
| 109 | **gitrob configuration file**<br><small>__</small> | <samp>filename</samp> |

<!-- END:(@data-leakage-protection/signatures table) -->

## 2. Install

**Before you begin**, you'll need to have these
>
> -   _**Programming languages:**_
>
>     -  [Node.js ![external][octicon-link-external]](https://nodejs.org/)  <samp>>= 10.x</samp>
>
>     -  [NPM ![external][octicon-link-external]](https://npmjs.com/) <samp>>= 4.0.0</samp>
>
> -   _**Skills:**_
>
>     You'll need to know how to access the [command line (aka, "Terminal") ![external][octicon-link-external]](https://www.codecademy.com/learn/learn-the-command-line) on your machine.

![Terminal][octicon-terminal] Open a Terminal and enter the following command:

```shell
# As a dependency in your Node.js app
npm i @data-leakage-protection/signatures --save-prod
```

## 3. Usage

Use `@data-leakage-protection/signatures.signatures` to find file extensions, names, and paths
that commonly leak secrets.

```js
const { signatures } = require('@data-leakage-protection/signatures')
// ⚠️ Note: the 'recursive-readdir' module is not bundled with
//    @data-leakage-protection/signatures. 'recursive-readdir' is referenced
//    only as an example.
const recursiveReaddir = require('recursive-readdir')

const potentialLeaks = recursiveReaddir('/path/to/local/repo')
  .then(files => files
    .map(file => signatures
    .map(signature => signature.match(file)))
  )
  .catch(err => err)
```

## 4. API

The **@data-leakage-protection/signatures** module provides a
`Signatures` class, which validates @data-leakage-protection/signatures and
converts regular expression strings to RE2 (whenever possible).

The **@data-leakage-protection/signatures** module's public API provides:

1.  `factory` method: a convenience function that creates a signature object.
1.  `nullSignature`: implements a default object literal with all signatures
    properties set to `null`.
1.  `Signature`: a class that constructs a signature object.
1.  `signatures`: an array of `Signature` instances.
1.  `toArray(data: {String|Array.<Object>})`: generates an `Array.<Signature>`
    from a JSON string or object literal array.
1.  `validParts`: a constants enum of valid `Signature.prototype.part` values.
1.  `validTypes`: a constants enum of valid `Signature.prototype.type` values.

### 4.1. `@data-leakage-protection/signatures.Signature`

A class that constructs Signature objects.

```js
const { Signature, validParts, validTypes } = require('@data-leakage-protection/signatures')

const signature = new Signature({
  caption: 'Potential cryptographic private key',
  description: '',
  part: validParts.EXTENSION,
  pattern: '.pem',
  type: validTypes.MATCH
})
```

### 4.2. `@data-leakage-protection/signatures.Signature.prototype.match`

Discover possible data leaks by `match`ing a Signature pattern
against file extensions, names, and paths.

```js
const rsaTokenSignature = new Signature({
  'caption': 'Private SSH key',
  'description': '',
  'part': 'filename',
  'pattern': '^.*_rsa$',
  'type': 'regex'
})

const suspiciousFilePath = '/hmm/what/might/this/be/id_rsa'
rsaTokenSignature.match(suspiciousFilePath)
// => ['/hmm/what/might/this/be/id_rsa']

const fileThatIsJustBeingCoolBruh = 'file/that/is/just/being/cool/bruh'
rsaTokenSignature.match(suspiciousFilePath)
// => null
```

![source code][octicon-code] [Review the source code for `signature`.](src/signature/index.js)

## 5. Accessing signatures with other tools and programming languages

You can access `signatures.json` without the **@data-leakage-protection/signatures**
Node module. Select a tool or programming language below to view examples.

<details><summary><strong>cURL</strong></summary>

You can access data-loss rules using HTTPS. You can <samp>GET</samp> all
signatures directly from Gitlab with cURL.

```shell
curl -X GET \
  'https://gitlab.com/data-leakage-protection/signatures/raw/master/signatures.json'
```

</details>

---

<details><summary><strong>Golang</strong></summary>

```golang
package main

import (
	"fmt"
	"net/http"
	"io/ioutil"
)

func main() {

	url := "https://gitlab.com/data-leakage-protection/signatures/raw/master/signatures.json"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("Private-Token", "<your-personal-token>")
	req.Header.Add("cache-control", "no-cache")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

}
```

</details>

---

<details><summary><strong>Java</strong> (OK HTTP)</summary>

```java
OkHttpClient client = new OkHttpClient();

String signaturesJson = "https://gitlab.com/data-leakage-protection/signatures/raw/master/signatures.json";

Request request = new Request.Builder()
  .url(signaturesJson)
  .get()
  .addHeader("Accept", "*/*")
  .addHeader("Cache-Control", "no-cache")
  .addHeader("Host", "gitlab.com")
  .addHeader("accept-encoding", "gzip, deflate")
  .addHeader("Connection", "keep-alive")
  .addHeader("cache-control", "no-cache")
  .build();

Response response = client.newCall(request).execute();

```

</details>

---

<details><summary><strong>Node</strong> (native)</summary>

```js
const http = require('https')

const options = {
  method: 'GET',
  hostname: ['gitlab', 'com'],
  path: ['api', 'v4', 'projects'],
  headers: {
    'Private-Token': '<your-access-token>',
    'cache-control': 'no-cache'
  }
}

const req = http.request(options, res => {
  const chunks = []

  res.on('data', chunk => {
    chunks.push(chunk)
  })

  res.on('end', () => {
    var body = Buffer.concat(chunks)
    console.log(body.toString())
  })
})

req.end()
```

</details>

---

<details><summary><strong>Python</strong> (versions 2 and 3)</summary>

Python3

```python
import http.client

conn = http.client.HTTPConnection("gitlab,com")

payload = ""

headers = {
  'Accept': "application/json",
  'cache-control': "no-cache"
}

conn.request("GET", "data-leakage-protection/signatures,raw,master,signatures.json", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

Python2

```python
import requests

url = "https://gitlab.com/data-leakage-protection/signatures/raw/master/signatures.json"

payload = ""
headers = {
  'Accept': "application/json",
  'cache-control': "no-cache"
}

response = requests.request("GET", url, data=payload, headers=headers)

print(response.text)
```

</details>

---

<details><summary><strong>Ruby</strong> (NET::Http)</summary>

```ruby
require 'uri'
require 'net/http'

url = URI("'https://gitlab.com/data-leakage-protection/signatures/raw/master/signatures.json")

http = Net::HTTP.new(url.host, url.port)

request = Net::HTTP::Get.new(url)
request["Private-Token"] = '<your-personal-token>'
request["cache-control"] = 'no-cache'

response = http.request(request)
puts response.read_body
```

</details>

## 6. Maintainers

[@gregswindle](https://github.com/gregswindle)

> ![Information for Maintainers][octicon-info] The
> [Maintainer Guide](./docs/maintainer-guide/README.md) has useful information
> for Maintainers and Trusted Committers.

## 7. Contributions

We gratefully accept Merge Requests! Here's what you need to know to get started.

> ![Before submitting a Merge Request, please read][octicon-heart] Before submitting a Merge Request, please read our:
> -  [Code of Conduct](CODE_OF_CONDUCT.md)
> -  [Contributing Aggreement](CONTRIBUTING.md)
> -  [Developer Guide](docs/developer-guide#README.md)
> -  [Maintainer/Trusted Committer Guide](docs/maintainer-guide#readme)

[![All Contributors](https://img.shields.io/badge/all_contributors-4-orange.svg?style=flat-square)](#contributors)
[![FOSSA Status](https://app.fossa.io/api/projects/custom%2B804%2Fgitlab.com%2Fdata-leakage-protection%2Fsignatures.svg?type=shield)](https://app.fossa.io/projects/custom%2B804%2Fgitlab.com%2Fdata-leakage-protection%2Fsignatures?ref=badge_shield)
[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![conventional commits](https://img.shields.io/badge/conventional%20commits-1.0.0-yellow.svg?style=flat-square)](https://www.conventionalcommits.org/en/v1.0.0-beta.2/#specification)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg?style=flat-square)](https://standardjs.com)

Thanks goes to our awesome contributors ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore -->
<table><tr><td align="center"><a href="https://gitlab.com/semantic-release-bot"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/3205456/avatar.png" width="100px;" alt="Semantic Release Bot"/><br /><sub><b>Semantic Release Bot</b></sub></a><br /><a href="#maintenance-semantic-release-bot" title="Maintenance">🚧</a></td><td align="center"><a href="https://gitlab.com/gregswindle"><img src="https://secure.gravatar.com/avatar/63ce0b39d728003b304d7a5aaff754e3?s=80&d=identicon" width="100px;" alt="gregswindle"/><br /><sub><b>gregswindle</b></sub></a><br /><a href="https://gitlab.com/data-leakage-protection/signatures/commits/master" title="Code">💻</a> <a href="https://gitlab.com/data-leakage-protection/signatures/commits/master" title="Tests">⚠️</a> <a href="https://gitlab.com/data-leakage-protection/signatures/commits/master" title="Documentation">📖</a> <a href="https://gitlab.com/data-leakage-protection/signatures/issues?author_username=gregswindle" title="Bug reports">🐛</a> <a href="#maintenance-gregswindle" title="Maintenance">🚧</a></td><td align="center"><a href="https://gitlab.com/christinavaldes"><img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/3446647/avatar.png" width="100px;" alt="Christina Valdes"/><br /><sub><b>Christina Valdes</b></sub></a><br /><a href="#review-christinavaldes" title="Reviewed Pull Requests">👀</a></td><td align="center"><a href="https://gitlab.com/spooraj"><img src="https://secure.gravatar.com/avatar/8d17097b6510950f5939baa68aa4ee96?s=80&d=identicon" width="100px;" alt="sairam pooraj"/><br /><sub><b>sairam pooraj</b></sub></a><br /><a href="#review-spooraj" title="Reviewed Pull Requests">👀</a></td></tr></table>

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!

### 7.1. Adding a Signature

Before adding a new Signature, please review all current definitions: the
Signature might already exist.

If the Signature does _not_ exist, please be sure to add your Signature with the
following properties:

1.  `caption`: A succinct summary for the Signature. Think of **caption** as a
    well-written email subject.

1.  `description`: Provide more details about the Signature if necessary.
    **description** is especially useful for differentiating similar Signatures.

1.  `hash`: A hexidecimal SHA256 representation of a Signature (with ordered properties).

1.  `name`: The Signature's `caption`, converted to kebab-case.

1.  `part`: An enumeration that defines _what_ the Signature is evaluating.
    Valid values are:

    - `contents`: The string(s) within a file.
    - `extension`: A file extension (which defines the Content-Type or
      mime-type).
    - `filename`: The unique name of the file.
    - `path`: The directory path _relative to the repo_ and _without_ the
      filename.

1.  `pattern`: The string or regular expression to look for.

1.  `type`: An enumeration that defines _how to_ evaluate for secrets. Valid
    values are:
    - `match`: A strict string equivalency evaluation.
    - `regex`: A regular expression "search" or "test".

### 7.2. Editing a Signature

Edits are welcome! Just be sure to unit test.

### 7.3. Removing a Signature

Please provide a testable justification for any Signature removal.

## 8. License

 [![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) © 2019 [Greg Swindle](https://gitlab.com/gregswindle).

[![FOSSA Status](https://app.fossa.io/api/projects/custom%2B804%2Fgitlab.com%2Fdata-leakage-protection%2Fsignatures.svg?type=large)](https://app.fossa.io/projects/custom%2B804%2Fgitlab.com%2Fdata-leakage-protection%2Fsignatures?ref=badge_large)

[View detailed legal **NOTICE**s ![View all FOSS legal notices][octicon-link-external]](https://app.fossa.io/reports/70f953b6-4269-4d85-b138-7e64c58150dc).

## 9. References and Attributions

[^1]: _What is Data Leakage? Defined, Explained, and Explored | Forcepoint._ (2019)
Retrieved January 27, 2019, from <https://www.forcepoint.com/cyber-edu/data-leakage>

<!-- ⛔️ Link references ⛔️  -->

[forcepoint-defn]: https://www.forcepoint.com/cyber-edu/data-leakage

[npmjs-about-modules-doc]: https://docs.npmjs.com/about-packages-and-modules#about-modules

[npmjs-about-packages-doc]: https://docs.npmjs.com/about-packages-and-modules#about-packages

[npmjs-public-registry-doc]: https://docs.npmjs.com/about-the-public-npm-registry

[semver-spec]: https://semver.org "Semantic Versioning 2.0.0 specification"


[gl-push-rules-doc]: https://docs.gitlab.com/ee/push_rules/push_rules.html
[gl-push-rules-api-docs]:
  https://docs.gitlab.com/ee/api/projects.html#push-rules-starter
[octicon-alert]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/alert.svg
[octicon-arrow-down]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-down.svg
[octicon-arrow-left]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-left.svg
[octicon-arrow-right]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-right.svg
[octicon-arrow-small-down]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-small-down.svg
[octicon-arrow-small-left]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-small-left.svg
[octicon-arrow-small-right]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-small-right.svg
[octicon-arrow-small-up]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-small-up.svg
[octicon-arrow-up]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/arrow-up.svg
[octicon-beaker]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/beaker.svg
[octicon-bell]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/bell.svg
[octicon-bold]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/bold.svg
[octicon-book]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/book.svg
[octicon-bookmark]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/bookmark.svg
[octicon-briefcase]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/briefcase.svg
[octicon-broadcast]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/broadcast.svg
[octicon-browser]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/browser.svg
[octicon-bug]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/bug.svg
[octicon-calendar]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/calendar.svg
[octicon-check]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/check.svg
[octicon-checklist]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/checklist.svg
[octicon-chevron-down]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/chevron-down.svg
[octicon-chevron-left]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/chevron-left.svg
[octicon-chevron-right]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/chevron-right.svg
[octicon-chevron-up]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/chevron-up.svg
[octicon-circle-slash]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/circle-slash.svg
[octicon-circuit-board]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/circuit-board.svg
[octicon-clippy]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/clippy.svg
[octicon-clock]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/clock.svg
[octicon-cloud-download]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/cloud-download.svg
[octicon-cloud-upload]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/cloud-upload.svg
[octicon-code]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/code.svg
[octicon-comment-discussion]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/comment-discussion.svg
[octicon-comment]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/comment.svg
[octicon-credit-card]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/credit-card.svg
[octicon-dash]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/dash.svg
[octicon-dashboard]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/dashboard.svg
[octicon-database]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/database.svg
[octicon-desktop-download]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/desktop-download.svg
[octicon-device-camera-video]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/device-camera-video.svg
[octicon-device-camera]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/device-camera.svg
[octicon-device-desktop]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/device-desktop.svg
[octicon-device-mobile]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/device-mobile.svg
[octicon-diff-added]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff-added.svg
[octicon-diff-ignored]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff-ignored.svg
[octicon-diff-modified]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff-modified.svg
[octicon-diff-removed]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff-removed.svg
[octicon-diff-renamed]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff-renamed.svg
[octicon-diff]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/diff.svg
[octicon-ellipses]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/ellipses.svg
[octicon-ellipsis]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/ellipsis.svg
[octicon-eye]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/eye.svg
[octicon-file-binary]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-binary.svg
[octicon-file-code]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-code.svg
[octicon-file-directory]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-directory.svg
[octicon-file-media]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-media.svg
[octicon-file-pdf]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-pdf.svg
[octicon-file-submodule]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-submodule.svg
[octicon-file-symlink-directory]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-symlink-directory.svg
[octicon-file-symlink-file]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-symlink-file.svg
[octicon-file-text]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-text.svg
[octicon-file-zip]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file-zip.svg
[octicon-file]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/file.svg
[octicon-flame]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/flame.svg
[octicon-fold]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/fold.svg
[octicon-gear]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/gear.svg
[octicon-gift]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/gift.svg
[octicon-gist-secret]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/gist-secret.svg
[octicon-gist]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/gist.svg
[octicon-git-branch]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-branch.svg
[octicon-git-commit]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-commit.svg
[octicon-git-compare]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-compare.svg
[octicon-git-merge]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-merge.svg
[octicon-git-pull-request]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/git-pull-request.svg
[octicon-globe]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/globe.svg
[octicon-grabber]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/grabber.svg
[octicon-graph]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/graph.svg
[octicon-heart]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/heart.svg
[octicon-history]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/history.svg
[octicon-home]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/home.svg
[octicon-horizontal-rule]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/horizontal-rule.svg
[octicon-hubot]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/hubot.svg
[octicon-inbox]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/inbox.svg
[octicon-info]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/info.svg
[octicon-issue-closed]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/issue-closed.svg
[octicon-issue-opened]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/issue-opened.svg
[octicon-issue-reopened]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/issue-reopened.svg
[octicon-italic]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/italic.svg
[octicon-jersey]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/jersey.svg
[octicon-key]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/key.svg
[octicon-keyboard]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/keyboard.svg
[octicon-law]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/law.svg
[octicon-light-bulb]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/light-bulb.svg
[octicon-link-external]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/link-external.svg
[octicon-link]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/link.svg
[octicon-list-ordered]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/list-ordered.svg
[octicon-list-unordered]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/list-unordered.svg
[octicon-location]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/location.svg
[octicon-lock]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/lock.svg
[octicon-logo-gist]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/logo-gist.svg
[octicon-logo-github]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/logo-github.svg
[octicon-mail-read]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mail-read.svg
[octicon-mail-reply]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mail-reply.svg
[octicon-mail]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mail.svg
[octicon-mark-github]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mark-github.svg
[octicon-markdown]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/markdown.svg
[octicon-megaphone]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/megaphone.svg
[octicon-mention]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mention.svg
[octicon-milestone]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/milestone.svg
[octicon-mirror]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mirror.svg
[octicon-mortar-board]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mortar-board.svg
[octicon-mute]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/mute.svg
[octicon-no-newline]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/no-newline.svg
[octicon-octoface]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/octoface.svg
[octicon-organization]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/organization.svg
[octicon-package]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/package.svg
[octicon-paintcan]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/paintcan.svg
[octicon-pencil]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/pencil.svg
[octicon-person]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/person.svg
[octicon-pin]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/pin.svg
[octicon-plug]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/plug.svg
[octicon-plus-small]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/plus-small.svg
[octicon-plus]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/plus.svg
[octicon-primitive-dot]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/primitive-dot.svg
[octicon-primitive-square]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/primitive-square.svg
[octicon-pulse]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/pulse.svg
[octicon-question]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/question.svg
[octicon-quote]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/quote.svg
[octicon-radio-tower]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/radio-tower.svg
[octicon-reply]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/reply.svg
[octicon-repo-clone]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo-clone.svg
[octicon-repo-force-push]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo-force-push.svg
[octicon-repo-forked]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo-forked.svg
[octicon-repo-pull]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo-pull.svg
[octicon-repo-push]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo-push.svg
[octicon-repo]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/repo.svg
[octicon-rocket]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/rocket.svg
[octicon-rss]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/rss.svg
[octicon-ruby]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/ruby.svg
[octicon-search]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/search.svg
[octicon-server]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/server.svg
[octicon-settings]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/settings.svg
[octicon-shield]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/shield.svg
[octicon-sign-in]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/sign-in.svg
[octicon-sign-out]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/sign-out.svg
[octicon-smiley]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/smiley.svg
[octicon-squirrel]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/squirrel.svg
[octicon-star]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/star.svg
[octicon-stop]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/stop.svg
[octicon-sync]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/sync.svg
[octicon-tag]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/tag.svg
[octicon-tasklist]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/tasklist.svg
[octicon-telescope]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/telescope.svg
[octicon-terminal]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/terminal.svg
[octicon-text-size]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/text-size.svg
[octicon-three-bars]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/three-bars.svg
[octicon-thumbsdown]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/thumbsdown.svg
[octicon-thumbsup]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/thumbsup.svg
[octicon-tools]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/tools.svg
[octicon-trashcan]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/trashcan.svg
[octicon-triangle-down]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/triangle-down.svg
[octicon-triangle-left]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/triangle-left.svg
[octicon-triangle-right]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/triangle-right.svg
[octicon-triangle-up]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/triangle-up.svg
[octicon-unfold]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/unfold.svg
[octicon-unmute]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/unmute.svg
[octicon-unverified]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/unverified.svg
[octicon-verified]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/verified.svg
[octicon-versions]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/versions.svg
[octicon-watch]:
  https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/watch.svg
[octicon-x]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/x.svg

[exploit-db-27527]: http://www.exploit-db.com/exploits/27527/

